module OutlineSelection exposing (Model, init, view, addPoint)

import Svg
import Svg.Attributes as SvgA


type alias Model =
    { id : Int
    , points : List (Int, Int)
    , highlighted : Bool
    }


init : Int -> List (Int, Int) -> Model
init id points =
    Model id points False


addPoint : Model -> (Int, Int) -> Model
addPoint model point =
    {model | points = point :: model.points}


view : Model -> Maybe String -> Maybe Int -> Svg.Svg msg
view model color strokewidth =
    let
        colorAttr = Maybe.withDefault "red" color
        strokewidthAttr = Maybe.withDefault "1" (Maybe.map toString strokewidth)
        opacityAttr = if model.highlighted then "0.5" else "0"
    in
        Svg.polygon
            [ SvgA.stroke colorAttr
            , SvgA.fill colorAttr
            , SvgA.strokeWidth strokewidthAttr
            , SvgA.fillOpacity opacityAttr
            , SvgA.points (stringPoints model.points)
            ] []


stringPoints : List (Int, Int) -> String
stringPoints points =
    let
        strList = List.map (\(x,y) -> toString x ++ "," ++ toString y) points
    in
        List.foldl (\x y -> x ++ " " ++ y) "" strList
