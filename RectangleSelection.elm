module RectangleSelection exposing
    ( Model, init
    , Msg, update, changeStyle
    , view
    , subscriptions
    )

import Svg
import Svg.Attributes as SvgA
import VirtualDom


main =
    VirtualDom.programWithFlags
        { init = (\(id, left, top, width, height) -> init id left top width height)
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


-- MODEL

type alias Model =
    { id : Int
    , left : Int
    , top : Int
    , width : Int
    , height : Int
    -- style
    , highlighted : Bool
    , color : String
    , strokeWidth : Int
    }


init : Int -> Int -> Int -> Int -> Int -> (Model, Cmd Msg)
init id left top width height =
    (Model id left top width height False "red" 1, Cmd.none)


-- UPDATE

type Msg
    = Style Bool String Int


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        Style highlighted color strokeWidth ->
            ( { model
                | highlighted = highlighted
                , color = color
                , strokeWidth = strokeWidth
              }
            , Cmd.none)


changeStyle : Model -> Maybe Bool -> Maybe String -> Maybe Int -> Msg
changeStyle model highlighted color strokeWidth =
    case highlighted of
        Nothing -> changeStyle model (Just model.highlighted) color strokeWidth
        Just h -> case color of
            Nothing -> changeStyle model (Just h) (Just model.color) strokeWidth
            Just c -> case strokeWidth of
                Nothing -> Style h c model.strokeWidth
                Just s -> Style h c s

-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


-- VIEW

view : Model -> Svg.Svg msg
view model =
    let
        opacityAttr = if model.highlighted then "0.5" else "0"
    in
        Svg.rect
            [ SvgA.stroke model.color
            , SvgA.fill model.color
            , SvgA.strokeWidth (toString model.strokeWidth)
            , SvgA.fillOpacity opacityAttr
            , SvgA.x (toString model.left)
            , SvgA.y (toString model.top)
            , SvgA.width (toString model.width)
            , SvgA.height (toString model.height)
            ] []
