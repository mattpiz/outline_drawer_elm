import Html exposing (..)
import Html.App as Html
import Html.Events exposing (onClick)
import Svg

import RectangleSelection as RSel
import OutlineSelection as OSel

main =
    Html.program
        { init = init
        , update = update
        , subscriptions = (\model -> Sub.none)
        , view = view
        }


-- MODEL


type alias Model =
  { rectangle : RSel.Model
  , outline : OSel.Model
  }


init : (Model, Cmd Msg)
init =
    let
        (rModel, rCmd) = RSel.init 0 10 20 30 40
        oModel = OSel.init 1 [(0,0), (10,10), (20,0), (20,20), (0,20)]
    in
        ( Model rModel oModel
        , Cmd.batch
            [ Cmd.map Rectangle rCmd
            , Cmd.none
            ]
        )


-- UPDATE


type Msg
    = Rectangle RSel.Msg
    | NoMsg


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        Rectangle rMsg ->
            let
                (rModel, rCmd) = RSel.update rMsg model.rectangle
            in
                ( Model rModel model.outline
                , Cmd.map Rectangle rCmd
                )
        NoMsg -> (model, Cmd.none)


-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ Svg.svg []
            [ RSel.view model.rectangle
            , OSel.view model.outline Nothing Nothing
            ]
        , button [ onClick (Rectangle (RSel.changeStyle model.rectangle Nothing Nothing (Just 5))) ] [ text "Bold" ]
        ]
